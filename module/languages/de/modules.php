<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:16:46+01:00
 */

global $TL_LANG;

$MOD = array(
    'avisota-subscription-member' => array(
        'Avisota - Bestellung für Mitglieder',
        'Avisota-Abonnements für Contao-Mitglieder',
    ),
);

$TL_LANG['MOD'] = array_merge(
    $TL_LANG['MOD'],
    $MOD
);
